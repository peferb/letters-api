function debug_print_updates(my_id, player_update) {
  console.log("My letters: ", player_update[my_id]);
  for (const player in player_update) {
    if (player == my_id) { continue; }
    console.log("Player:" + player + " Letters:" + player_update[player]);
  }
}

function update_gameboard(my_id, player_update) {
  // First we update our own
  my_letters = player_update[my_id];
  if (my_letters.length == 0) {
    document.getElementById("me").innerHTML = "My letters: <h2>DEAD</h2>";
  } else {
    document.getElementById("me").innerHTML = "My letters: <h2>" + my_letters + "</h2>";
  }
  no_opponents = Object.keys(player_update).length - 1;
  width_per_opponent = 100.0 
  if (no_opponents > 0) {
    width_per_opponent = 100.0 / no_opponents
  }
  opponents_string = ""
  for (const player in player_update) {
    if (player == my_id) { continue; }
    opponents_letters = player_update[player];
    opponents_string += '<div class="opponent_box" style="width:' + width_per_opponent +'%;float:left;">';
    opponents_string += 'Player: ' + player
    opponents_string += '<h2>'
    if (opponents_letters.length == 0) {
      opponents_string += " - DEAD";
    } else {
      opponents_string += " Letters: " + player_update[player];
    }
    opponents_string += '</h2></div>';
  }
  document.getElementById("opponents").innerHTML = opponents_string;
}

//var socket = io("http://localhost:5000");
var socket = io("https://lettersocket.herokuapp.com");
var my_id = null
socket.on('connect', function() {
  socket.emit('my event', {data: 'I\'m connected!'});
});
socket.on('init message', function(msg) {
    console.log("Init message:")
    console.log(msg);
    my_id = msg['id'];
});
socket.on('player update', function(player_update) {
  console.log(player_update);
  debug_print_updates(my_id, player_update);
  update_gameboard(my_id, player_update);
});
socket.on('status update', function(status_message) {
  document.getElementById("status").textContent = status_message;
});
socket.on('timer', function(msg) {
  console.log(msg)
  document.getElementById("countdown_timer").textContent = msg;
});

document.body.addEventListener("keydown", function(event) {
  socket.emit('key pressed', {"keycode": event.keyCode});
});

