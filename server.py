from flask import Flask, render_template, request
from flask_socketio import SocketIO, emit, send
import string
import random
import time

app = Flask(__name__)
app.config['SECRET_KEY'] = 'supersecretmuchwow!'
socketio = SocketIO(app, cors_allowed_origins="*", logger=True)

players = {}
players_nicknames = {}
players_scores = {}
available_letters = string.ascii_uppercase
letters_per_player = 3

def update_status():
    players_alive = 0
    players_total = 0
    players_alive_names = []
    for player, letters in players.items():
        players_total += 1
        if len(letters) > 0:
            players_alive += 1
            players_alive_names.append(player)
    if players_alive <= 1 and players_total > 1:
        status_message = f"Player {players_nicknames[players_alive_names[0]]} won the game!"
        players_scores[players_alive_names[0]] = players_scores[players_alive_names[0]] + 1
        emit('status update', status_message, broadcast=True)
        reset_game()
    elif players_total == 1:
        status_message = f"Waiting for more players..."
        emit('status update', status_message, broadcast=True)
    else:
        status_message = f"{players_alive} players still alive and playing!"
        emit('status update', status_message, broadcast=True)

def update_everything():
    update_status()
    broadcast_all_players_and_letters()
    broadcast_nicknames()
    update_scoreboard()

def update_scoreboard():
    scorestring = ""
    for player, _ in players.items():
        if player in players_nicknames:
            scorestring += f"{players_nicknames[player]}: {players_scores[player]}"
        else:
            scorestring += f"{player}: {players_scores[player]}"
        scorestring += "<br>"
    emit('score update', scorestring, broadcast=True)

def reset_game():
    print("Resetting game, counting down")
    emit('timer', "Reseting in 5...", broadcast=True)
    socketio.sleep(1)
    emit('timer', "Reseting in 4...", broadcast=True)
    socketio.sleep(1)
    emit('timer', "Reseting in 3...", broadcast=True)
    socketio.sleep(1)
    emit('timer', "Reseting in 2...", broadcast=True)
    socketio.sleep(1)
    emit('timer', "Reseting in 1...", broadcast=True)
    socketio.sleep(1)
    emit('timer', "", broadcast=True)
    print("Sleep done")
    # emit('countdown timer', "", broadcast=True)
    for player, letters in players.items():
        players[player] = random.choices(available_letters, k=letters_per_player)
    update_everything()


@socketio.on('key pressed')
def handle_keypress(data):
    # Check if use is still alive
    if len(players[request.sid]) == 0:
        return
    key_pressed = chr(data['keycode'])
    print(f"User {request.sid} pressed keycode: {data['keycode']} which is {key_pressed}")
    for player, letters in players.items():
        if key_pressed in letters:
            players[player].remove(key_pressed)
    update_everything()

@socketio.on('set nickname')
def handle_nickname(data):
    players_nicknames[request.sid] = data['nickname']

def broadcast_all_players_and_letters():
    emit('player update', players, broadcast=True)

def broadcast_nicknames():
    emit('player nicknames', players_nicknames, broadcast=True)

@socketio.on('connect')
def setup_player_on_connect(auth):
    print(f"New player with id: {request.sid}")
    # give them x letters
    player_letters = random.choices(available_letters, k=letters_per_player)
    players[request.sid] = player_letters 
    players_scores[request.sid] = 0
    emit('init message', {"id":request.sid}, room=request.sid)
    update_everything()

@socketio.on('disconnect')
def player_disconnect():
    print(f'Player disconnected, removing {request.sid} from game')
    players.pop(request.sid)
    players_nicknames.pop(request.sid)
    update_everything()

@app.route('/')
def index():
    return render_template('combined_index.html')

if __name__ == '__main__':
    socketio.run(app)

